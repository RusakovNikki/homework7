export const asyncFn = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve(
      <img
        width="100%"
        alt="content"
        src="https://markiv.pp.ua/blog/content/public/upload/everyprogrammerismusician_0_o.jpeg"
      />
    );
  }, 3300);
});
