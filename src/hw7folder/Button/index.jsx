import React from 'react';

import './styles.css';

export const Button = ({ children, onClick, color }) => (
  <button
    type="button"
    onClick={onClick}
    className={`Button-component ${color}`}
  >
    {children}
  </button>
);

Button.defaultProps = {
  color: 'red',
};
