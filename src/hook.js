import React from 'react';

export const useCustomHook = (context) => {
  const contextValue = React.useContext(context);

  React.useDebugValue('cutom context hook')

  return contextValue;
};
