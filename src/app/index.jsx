import React from 'react';

import { Button } from '../Button'
import { Root } from '../hw7folder/Root';

import './App.css';

function App(props) {
  const [state, setState] = React.useState(0);
  const ref = React.useRef(null);

  React.useEffect(() => {
    console.log(ref.current);
  }, []);

  const handleClick = React.useCallback((e) => {
    setState(prevState => prevState + 1);
  }, [])

  const flag = React.useMemo(() => (
    state ? 'yes' : 'net'
  ), [state]);

  return (
    <div className="App">
      <header className="App-header">
        <h1 ref={ref}>
          App.js - {state} - {flag}
        </h1>
        <Button onClick={handleClick}>
          button
        </Button>
      </header>
        <Root />
    </div>
  );
}

export default App;
