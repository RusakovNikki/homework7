import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './app';

import { HookContext } from './context';

ReactDOM.render(
  <React.StrictMode>
    <HookContext.Provider value={{ color: 'green' }}>
      <App />
    </HookContext.Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);
