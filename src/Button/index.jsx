import React from 'react';

import './style.css';
import { HookContext } from  '../context';
import { useCustomHook } from '../hook';

export const Button = ({ children, onClick }) => {
  const contextValue = useCustomHook(HookContext);

  return (
    <button
      type="button"
      onClick={onClick}
      className={`Button-component ${contextValue.color}`}
    >
      {children}
    </button>
  );
}

Button.defaultProps = {
  color: 'red',
};
